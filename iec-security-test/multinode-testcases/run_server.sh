#!/bin/sh

. ../lib/multinode-comm-lib

CLIENT_IP_ADDR=""
CLIENT_UN=""
CLIENT_UN_PWD=""

SERVER_IP_ADDR=""
SERVER_UN="seshu"
SERVER_UN_PWD="seshu"

execute_server_script(){
    msg=$1
    testcase_id=$(parse_msg $msg 1)
    testcase_arg=$(parse_msg $msg 2)

    res="fail"
    if [ -f $testcase_id/runTest-server.sh ];then
        cd $testcase_id
        eval ./runTest-server.sh "$testcase_arg"
        [ $? -eq 0 ] && res="success"
    fi
}

init_msgid
while true; do
    wait_for_client_msg
    client_msg=$(parse_msg $CLIENT_MSG 1)
    case $client_msg in
        "client_details")
            CLIENT_IP_ADDR="$(parse_msg $CLIENT_MSG 2)"
            CLIENT_UN="$(parse_msg $CLIENT_MSG 3)"
            CLIENT_UN_PWD="$(parse_msg $CLIENT_MSG 4)"
            send_ack_to_client "success"
            ;;
        "server_details")
            ip link set enp0s2 up && ip addr
            SERVER_IP_ADDR="$(ip addr | grep "state UP" -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')"
            send_ack_to_client $SERVER_IP_ADDR $SERVER_UN $SERVER_UN_PWD
            ;;
        "stop")
            echo "Client request to stop"
            send_ack_to_client "success"
            break
            ;;
        *)
            echo "Other message=$msg"
            execute_server_script $CLIENT_MSG
            send_ack_to_client "success"
            ;;
    esac
done

echo "Server Stopped"