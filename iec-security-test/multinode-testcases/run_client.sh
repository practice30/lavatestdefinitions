#!/bin/sh

. ../lib/multinode-comm-lib
CURPATH=`pwd`

ip link set enp0s2 up && ip addr
CLIENT_IP_ADDR="$(ip addr | grep "state UP" -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')"
CLIENT_UN="root"
CLIENT_UN_PWD="root"
echo "Host IP addr: ${CLIENT_IP_ADDR}"

send_msg_to_server "client_details" $CLIENT_IP_ADDR $CLIENT_UN $CLIENT_UN_PWD

send_msg_to_server "server_details"
echo "response from server: $RESP"

SERVER_IP_ADDR="$(parse_msg $RESP 1)"
SERVER_UN="$(parse_msg $RESP 2)"
SERVER_UN_PWD="$(parse_msg $RESP 3)"
echo "Remote ip addr: $SERVER_IP_ADDR"
echo "Remote username: $SERVER_UN"
echo "Remote password: $SERVER_UN_PWD"
if [ -z ${SERVER_IP_ADDR} ] || [ -z ${SERVER_UN} ] || [ -z ${SERVER_UN_PWD} ];then
        echo "Ip address or username or password can not be empty"
        exit 1
fi
#. ./run_all.sh -i ${SERVER_IP_ADDR} -u ${SERVER_UN} -p ${SERVER_UN_PWD}

for dir in *;
do
	[ ! -d ${CURPATH}/${dir} ] && continue
	echo $dir
    	cd ${CURPATH}/${dir}
	res="skip"
	if [ -f ./runTest-client.sh ]; then
		eval ./runTest-client.sh -i ${SERVER_IP_ADDR} -u ${SERVER_UN} -p ${SERVER_UN_PWD}
                [ $? -eq 0 ] && res="pass" || res="fail"
	fi
	which lava-test-case > /dev/null && lava-test-case ${dir} --result $res
done

echo "Test Done"
