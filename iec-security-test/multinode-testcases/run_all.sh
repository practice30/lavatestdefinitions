#!/bin/sh

CURPATH=`pwd`
ROLE="$(lava-role)"
RESULT_FILE="./result_file.txt"
IP=$1

echo "Ip address: " ${IP}

server_run()
{
	[ -f ${RESULT_FILE} ] && mv ${RESULT_FILE} ${RESULT_FILE}".bkp" || touch ${RESULT_FILE}

	# Send the server ip address and wa
	lava-send server-ip ip_addr=${IP}
	lava-wait client-ip
	client_ip=$(cat /tmp/lava_multi_node_cache.txt | cut -d = -f 2)

	for f in *;
	do
		[ ! -d ${CURPATH}/${f} ] && continue
		dir=${f}
		lava-send test-case-id id=$dir
		lava-wait test-case-ready
    		echo $dir
	    	cd ${CURPATH}/${dir}
    		if [ -f ./runtest_server.sh ]; then
	    		eval ./runtest_server.sh ${client_ip}
	    		if [ $? = "0" ]; then
		    		echo "${dir}+pass" >> ${CURPATH}/${RESULT_FILE}
	    		else
		    		echo "${dir}+fail" >> ${CURPATH}/${RESULT_FILE}
	    		fi
    		else
			echo "${dir}+skip" >> ${CURPATH}/${RESULT_FILE}
    		fi
	done

	lava-send test-case-id id="done"

	. ../send-to-lava.sh ./result_file.txt
}

client_run()
{
	# Wait for server ip address and send the client ip address
	lava-wait server-ip
	server_ip=$(cat /tmp/lava_multi_node_cache.txt | cut -d = -f 2)
	lava-send client-ip ip_addr=${IP}
	
	while :
	do
		lava-wait test-case-id
		dir=$(cat /tmp/lava_multi_node_cache.txt | cut -d = -f 2)
		echo $dir
		[ "${dir}" = "done" ] && break
    		cd ${CURPATH}/${dir}
		[ -f ./runtest_client.sh ] && eval ./runtest_client.sh ${server_ip}
		lava-send test-case-ready
		
	done
}

if [ "${ROLE}" = "server" ]; then
	server_run
else
	client_run
fi
