#!/bin/sh
# Security Testcase
# TC_CR1.1_2: Verify human user identification and authentication 
#   for remote interface

set -e
. ../../lib/common-lib

usage() {
        echo "Usage: $0 [-i REMOTE_IP_ADDR] [-u REMOTE_UN] [-p REMOTE_UN_PWD ]" 1>&2
        exit 1
}

while getopts "i:u:p:h" o; do
        case "$o" in
                i) REMOTE_IP_ADDR="${OPTARG}" ;;
                u) REMOTE_UN="${OPTARG}" ;;
                p) REMOTE_UN_PWD="${OPTARG}" ;;
                h|*) usage ;;
        esac
done

! check_root && error_msg "You need to be root to run this script."

pkg="openssh-server openssh-client passwd login sshpass"
! check_pkg "${pkg}" && error_msg ""${pkg}" package not enabled in Machine"

# Add local user
check_user ${LOCAL_UN}
add_user ${LOCAL_UN} ${LOCAL_UN_PWD}

res=0
if checkSSHFromRemote "$LOCAL_UN@$LOCAL_IP_ADDR" "$LOCAL_UN_PWD" \
			"$REMOTE_UN@$REMOTE_IP_ADDR" "$REMOTE_UN_PWD" ; then
	echo "Pass"
	res=0
else
	echo "Fail"
	res=1
fi

del_user ${LOCAL_UN}
exit $res
